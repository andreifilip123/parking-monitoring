namespace ParkingMonitoring.Interfaces
{
    public interface IVehicle
    {
        VehicleType Type { get; }
        void Park();
        void LeaveParking();
    }
}
