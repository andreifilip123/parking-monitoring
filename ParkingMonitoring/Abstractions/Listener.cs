﻿using System;
using System.Collections.Generic;
namespace ParkingMonitoring.Interfaces
{
    public abstract class Listener
    {
        public string _name { get; protected set; }
        protected ParkingLot _parkingLot { get; set; }

        public virtual void Update(ParkingLot parkingLot)
        {
            _parkingLot = parkingLot;
            DisplayParking();
        }

        public void SubscribeMe(ParkingLot parkingLot)
        {
            Console.WriteLine(string.Format("{0} subscribed to {1}", _name, parkingLot.ParkingName));
            parkingLot.AddListener(this);
        }

        public void UnSubscribeMe(ParkingLot parkingLot)
        {
            Console.WriteLine(string.Format("{0} unsubscribed to {1}", _name, parkingLot.ParkingName));
            parkingLot.RemoveListener(this);
        }

        protected abstract void DisplayParking();
    }
}
