﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ParkingMonitoring.Interfaces;

namespace ParkingMonitoring
{
    public class ParkingLot
    {
        public ParkingSpot[,] Spots { get; set; }
        public int Dimension { get; private set; }
        public string ParkingName { get; private set; }
        public List<Listener> Listeners { get; set; }

        public ParkingLot(int dimension, string name)
        {
            Dimension = dimension;
            ParkingName = name;
            Listeners = new List<Listener>();
            Spots = new ParkingSpot[dimension, dimension];
            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    Random random = new Random();
                    int typeIndex = random.Next(0, Enum.GetNames(typeof(VehicleType)).Length - 1);
                    Spots[i, j] = new ParkingSpot
                    {
                        Type = (VehicleType)typeIndex,
                        Row = i,
                        Column = j
                    };
                    Thread.Sleep(2);
                }
            }
        }

        public void AddListener(Listener listener)
        {
            Listeners.Add(listener);
            Console.WriteLine("{0} added listener {1}", ParkingName, listener._name);
        }

        public void RemoveListener(Listener listener)
        {
            Listeners.Remove(listener);
            Console.WriteLine("{0} removed listener {1}", ParkingName, listener._name);
        }

        public void Notify()
        {
            foreach (Listener listener in Listeners)
            {
                listener.Update(this);
            }
        }

        public ParkingSpot AssignSpot(VehicleType vehicleType)
        {
            var parkingSpot = FindAvailableParkingSpot(vehicleType);
            Spots[parkingSpot.Row, parkingSpot.Column].Occupy();
            Notify();
            return parkingSpot;
        }


        public void FreeSpot(ParkingSpot parkingSpot)
        {
            Spots[parkingSpot.Row, parkingSpot.Column].Free();
            Notify();
        }

        private ParkingSpot FindAvailableParkingSpot(VehicleType vehicleType)
        {
            for (int i = 0; i < Dimension; i++)
            {
                for (int j = 0; j < Dimension; j++)
                {
                    if (!Spots[i, j].IsOccupied && Spots[i,j].Type == vehicleType)
                    {
                        return Spots[i, j];
                    }
                }
            }

            return new ParkingSpot();
        }
    }
}
