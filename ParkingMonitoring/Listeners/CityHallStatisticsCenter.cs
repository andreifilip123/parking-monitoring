﻿using System;
using ParkingMonitoring.Interfaces;

namespace ParkingMonitoring.Listeners
{
    public class CityHallStatisticsCenter: Listener
    {
        public CityHallStatisticsCenter(string name)
        {
            _name = name;
        }

        public void DisplayStatistics()
        {
            int parkingCapacity = _parkingLot.Dimension * _parkingLot.Dimension;
            int availableSpots = CountFreeSpots();

            var occupancy = ((double)availableSpots / (double)parkingCapacity) * 100;

            Console.WriteLine();
            Console.WriteLine(string.Format("ParkingName: {0} {1}Total number of Spots: {2}{3}Total number of Available Spots:{4}{5}Occupancy: {6}%{7}",
                              _parkingLot.ParkingName, Environment.NewLine, parkingCapacity, Environment.NewLine, availableSpots, Environment.NewLine, occupancy, Environment.NewLine));
        }

        private int CountFreeSpots()
        {
            int count = 0;

            for (int i = 0; i < _parkingLot.Dimension; i++)
            {
                for (int j = 0; j < _parkingLot.Dimension; j++)
                {
                    if (!_parkingLot.Spots[i, j].IsOccupied)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        protected override void DisplayParking()
        {
            Console.WriteLine(string.Format("{0} status updated on {1} {2} ", _parkingLot.ParkingName, _name, Environment.NewLine));

            for (int i = 0; i < _parkingLot.Dimension; i++)
            {
                for (int j = 0; j < _parkingLot.Dimension; j++)
                {
                    if (_parkingLot.Spots[i, j].IsOccupied)
                    {
                        Console.Write("1 ");

                    }
                    else
                    {
                        Console.Write("0 ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
