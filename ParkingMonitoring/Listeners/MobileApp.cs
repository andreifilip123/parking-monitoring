﻿using System;
using ParkingMonitoring.Interfaces;

namespace ParkingMonitoring.Listeners
{
    public class MobileApp : Listener
    {
        private ParkingSpot _parkingSpot { get; set; }
        private IVehicle _vehicle { get; set; }

        public MobileApp(string name)
        {
            _name = name;
        }

        public void AddVehicle(IVehicle vehicle)
        {
            _vehicle = vehicle;
        }

        private void RequstParkingSpot(ParkingLot parkingLot)
        {
            _parkingSpot = parkingLot.AssignSpot(_vehicle.Type);
        }

        public void Park(ParkingLot parkingLot)
        {
            RequstParkingSpot(parkingLot);
            _vehicle.Park();
            Console.WriteLine(string.Format("I am parking at Row: {0} Parking Spot no: {1}", _parkingSpot.Row, _parkingSpot.Column));
        }

        public void LeaveParking(ParkingLot parkingLot)
        {
            _vehicle.LeaveParking();
            parkingLot.FreeSpot(_parkingSpot);
        }

        protected override void DisplayParking()
        {
            Console.WriteLine(string.Format("{0} status updated on {1} {2} ", _parkingLot.ParkingName, _name, Environment.NewLine));

            for (int i = 0; i < _parkingLot.Dimension; i++)
            {
                for (int j = 0; j < _parkingLot.Dimension; j++)
                {
                    if (_parkingLot.Spots[i, j].IsOccupied)
                    {
                        Console.Write("1 ");
                    }
                    else
                    {
                        Console.Write("0 ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
