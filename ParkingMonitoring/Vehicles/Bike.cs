using System;

namespace ParkingMonitoring.Interfaces
{
    public class Bike : IVehicle
    {
        public VehicleType Type => VehicleType.Bike;

        public void LeaveParking()
        {
            Console.WriteLine("I'm a bike and I'm leaving this parking lot");
        }

        public void Park()
        {
            Console.WriteLine("I'm a bike and I'm parking on a special spot for bikes");
        }
    }
}
