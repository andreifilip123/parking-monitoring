using System;

namespace ParkingMonitoring.Interfaces
{
    public class ElectricCar : IVehicle
    {
        public VehicleType Type => VehicleType.ElectricCar;

        public void LeaveParking()
        {
            Console.WriteLine("I'm an electric car and I'm leaving this parking lot");
        }

        public void Park()
        {
            Console.WriteLine("I'm an electric car and I'm parking and charging");
        }
    }
}
