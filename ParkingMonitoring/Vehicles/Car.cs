using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMonitoring.Interfaces
{
    public class Car : IVehicle
    {
        public VehicleType Type => VehicleType.Car;

        public void LeaveParking()
        {
            Console.WriteLine("I'm a car and I'm leaving this parking lot");
        }

        public void Park()
        {
            Console.WriteLine("I'm a car and I'm parking");
        }
    }
}
