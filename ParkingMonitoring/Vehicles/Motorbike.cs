using System;

namespace ParkingMonitoring.Interfaces
{
    public class Motorbike : IVehicle
    {
        public VehicleType Type => VehicleType.Motorbike;

        public void LeaveParking()
        {
            Console.WriteLine("I'm a motorbike car and I'm leaving this parking lot");
        }

        public void Park()
        {
            Console.WriteLine("I'm a motorbike and I'm parking on a special spot for motorbikes");
        }
    }
}
