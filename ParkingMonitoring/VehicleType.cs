﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMonitoring
{
    public enum VehicleType
    {
        Car = 0,
        ElectricCar = 1,
        Motorbike = 2,
        Bike = 3
    }
}
