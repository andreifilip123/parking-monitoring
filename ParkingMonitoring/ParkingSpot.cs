﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMonitoring
{
    public class ParkingSpot
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public bool IsOccupied { get; set; }
        public VehicleType Type { get; set; }

        public ParkingSpot()
        {
            IsOccupied = false;
            Type = VehicleType.Car;
        }

        public void Occupy()
        {
            IsOccupied = true;
        }

        public void Free()
        {
            IsOccupied = false;
        }
    }
}
