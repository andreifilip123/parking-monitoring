﻿using System;
using ParkingMonitoring.Interfaces;
using ParkingMonitoring.Listeners;

namespace ParkingMonitoring
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingLot CentralParking = new ParkingLot(10, "Central Parking");

            MobileApp mobileMock = new MobileApp("Android Device #1 Samsung");
            CarNavigation carNavigationMock = new CarNavigation("Integrated car navigation #1 VW");
            CityHallStatisticsCenter cityHallStatisticsMock = new CityHallStatisticsCenter("Sibiu City Town Hall Statistics Center");

            CentralParking.AddListener(mobileMock);
            CentralParking.AddListener(carNavigationMock);
            CentralParking.AddListener(cityHallStatisticsMock);

            mobileMock.AddVehicle(new Car());
            mobileMock.Park(CentralParking);

            mobileMock.AddVehicle(new Motorbike());
            mobileMock.Park(CentralParking);

            carNavigationMock.AddVehicle(new ElectricCar());
            carNavigationMock.Park(CentralParking);

            mobileMock.LeaveParking(CentralParking);
            carNavigationMock.LeaveParking(CentralParking);

            cityHallStatisticsMock.DisplayStatistics();

            mobileMock.UnSubscribeMe(CentralParking);
            mobileMock.SubscribeMe(CentralParking);

            Console.Read();
        }
    }
}
